# vim:syntax=apparmor
#
# Copyright (C) 2012-2015 Collabora Ltd.
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 2 of the GNU General Public
# License published by the Free Software Foundation.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

###
# <abstractions/chaiwala-media>: allow reading non-app-specific documents
#
# This abstraction gives the confined process read access to:
#
# * files in the well-known per-user XDG special directories for videos,
#   music, pictures and general documents
# * files in the analogous directories that are shared between all users
# * removable media such as USB thumb drives
# * test data installed system-wide
#
# It approximately corresponds to android.permission.READ_EXTERNAL_STORAGE.
#
# Status: Apertis-specific
# Privilege level: slightly elevated privilege
# Typical users: multimedia app-bundles
# Dependencies: <tunables/global>
###

  # Allow reading and locking media files in various places
  owner @{HOME}/Videos/				rk,
  owner @{HOME}/Videos/**			rk,
  owner @{HOME}/Music/				rk,
  owner @{HOME}/Music/**			rk,
  owner @{HOME}/Documents/			rk,
  owner @{HOME}/Documents/**			rk,
  owner @{HOME}/Pictures/			rk,
  owner @{HOME}/Pictures/**			rk,
  owner /media/*/**				rk,
  owner /run/media/*/**				rk,

  # These shared files are allowed even if you don't own them
  /home/shared/Videos/				rk,
  /home/shared/Videos/**			rk,
  /home/shared/Music/				rk,
  /home/shared/Music/**				rk,
  /home/shared/Documents/			rk,
  /home/shared/Documents/**			rk,
  /home/shared/Pictures/			rk,
  /home/shared/Pictures/**			rk,
